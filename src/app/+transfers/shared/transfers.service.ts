/**
 * Created by tinku on 6/1/17.
 */
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx'

import {Outlook} from './outlook'
import {OutlookMessage} from "./outlook-message.class";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

import {JsonApiService} from "../../core/api/json-api.service";

@Injectable()
export class TransfersService {

    public activeFolder: Subject<any>;

    public messages: Subject<any>;

    private state = {
        lastFolder: '',
        messages: []
    };

    constructor(private jsonApiService: JsonApiService) {
        this.activeFolder = new Subject();
        this.messages = new Subject();
    }


    getMessages(folder: string) {



    }

    getMessage(id: string) {

    }

    deleteSelected() {

        this.messages.next(this.state.messages.filter((it)=>!it.selected))
    }

}

