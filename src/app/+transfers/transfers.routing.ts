/**
 * Created by tinku on 6/1/17.
 */

import {Routes, RouterModule} from "@angular/router";
import {ComposeComponent} from "./compose/compose.component";

export const routes: Routes = [
    {
        path: 'compose',
        component: ComposeComponent,
        data: {pageTitle: 'Compose'}

    },

];


export const routing = RouterModule.forChild(routes);

