/**
 * Created by tinku on 6/1/17.
 */


import { NgModule } from '@angular/core';

import { SmartadminModule } from '../shared/smartadmin.module'

import { routing } from './transfers.routing';

import {ComposeComponent} from "./compose/compose.component";
import {SmartadminEditorsModule} from "../shared/forms/editors/smartadmin-editors.module";

@NgModule({
    declarations: [
        ComposeComponent
    ],
    imports: [


        SmartadminModule,
        routing,

        SmartadminEditorsModule,
    ]
})
export class TransfersModule {

}

