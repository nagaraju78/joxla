/**
 * Created by tinku on 6/1/17.
 */
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {OutlookMessage} from "../shared/outlook-message.class";
import {OutlookService} from "../shared/outlook.service";

import {FadeInLeft} from "../../shared/animations/fade-in-left.decorator";


@FadeInLeft()
@Component({
    selector: 'sa-compose',
    templateUrl: './compose.template.html',
})
export class ComposeComponent implements OnInit {

    public carbonCopy:boolean = false;
    public blindCarbonCopy:boolean = false;
    public attachments:boolean = false;

    public sending:boolean = false;

    constructor(private route:ActivatedRoute,
                private router:Router) {

    }

    ngOnInit(){

    }

    send() {
        this.sending = true;
    }
}
